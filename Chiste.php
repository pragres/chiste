<?php

class Chiste extends Service
{
	/**
	 * Load the Service when an email request arrives
	 * 
	 * @param Request
	 * */
	public function main($request){
		// search for the lyric
		$joke = $this->searchForAJokeOnTheInternet();

		// create a json object to send to the template
		$json = '
			{
				"title": "'.$joke['title'].'",
				"content": "'.$joke['content'].'",
				"reference": "'.$joke['reference'].'"
			}
		';

		// configure the response object to use a user defined template
		$this->response->createUserDefinedResponse("basic.tpl", $json);
	}

	/**
	 * Search for a joke on the Internet as an HTML
	 * 
	 * @author salvipascual
	 * @return array(title, content, reference)
	 * */
	private function searchForAJokeOnTheInternet(){
		// read to the jokes rss
		$xml = @file_get_contents("http://www.chistesdiarios.com/rss.xml");

		// parse the XML into an array of jokes
		$xml = simplexml_load_string($xml);
		if (isset($xml->channel->item)){
			$jokes = $xml->channel->item;

			// get a random joke from the list
			$jokeNumber = rand(0,count($jokes));
			$joke = $jokes[$jokeNumber];

			// create response array
			return array(
				"title"=>(String)$joke->title,
				"content"=>(String)$joke->description,
				"reference"=>(String)$joke->link
			);
		}
	}
}